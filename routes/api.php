<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('formget','formcontroller@formget');
Route::post('formpost','formcontroller@formpost');
Route::get('getdata','formcontroller@getdata');
Route::get('DeleteData/{id}','formcontroller@DeleteData');
Route::get('editform/{id}','formcontroller@editform');
Route::post('updateform','formcontroller@updateform');
